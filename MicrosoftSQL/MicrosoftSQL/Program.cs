﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MicrosoftSQL
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            SqlCommand comando;
            SqlConnection conexion;
            Queue<Patente> patentes = new Queue<Patente>();
            patentes.Enqueue(new Patente("AJ111FG",Patente.Tipo.Mercosur));
            patentes.Enqueue(new Patente("AJ127DF",Patente.Tipo.Mercosur));
            patentes.Enqueue(new Patente("AJ123DO",Patente.Tipo.Mercosur));
            patentes.Enqueue(new Patente("AJ123DQ",Patente.Tipo.Mercosur));
            
            using (conexion = new SqlConnection("Data Source=DESKTOP-KG7HBVI;Initial Catalog=patentes-sp-2018;Integrated Security=True"))
            {
                using (comando = new SqlCommand())
                {
                    
                    string insert =
                        "INSERT INTO [patentes-sp-2018].dbo.Patentes (patente, tipo) VALUES (@patente,@tipo)";
                    comando.Connection = conexion;
                    comando.CommandType = CommandType.Text;
                    comando.CommandText = insert;
                    comando.Parameters.Add("@patente", SqlDbType.VarChar);
                    comando.Parameters.Add("@tipo", SqlDbType.VarChar);
                    conexion.Open();
                    foreach (Patente patente in patentes)
                    {
                        comando.Parameters["@patente"].Value = patente.CodigoPatente;
                        comando.Parameters["@tipo"].Value = patente.TipoCodigo.ToString();
                    }
                    conexion.Close();
                }
            }
         
        }
    }
    public class Patente
    {
        public enum Tipo
        {
            Vieja, Mercosur
        }
        private Tipo tipoCodigo;
        private string codPatente;

        public Patente() { }

        public Patente(string codPatente, Tipo tipo)
        {
            this.CodigoPatente = codPatente;
            this.TipoCodigo = tipo;
        }

        public string CodigoPatente
        {
            get
            {
                return this.codPatente;
            }
            set
            {
                this.codPatente = value;
            }
        }

        public Tipo TipoCodigo
        {
            get
            {
                return this.tipoCodigo;
            }
            set
            {
                this.tipoCodigo = value;
            }
        }

        public override string ToString()
        {
            return this.CodigoPatente;
        }
    }
}